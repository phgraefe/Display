//-----------------------------------------------------
//File: oledm204.h
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------

#ifndef OLEDM204_H_
#define OLEDM204_H_


#define OLED_CS_LOW		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);
#define OLED_CS_HIGH	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET);

// ***********************************************************************
#define TOPVIEW 	"0x05"
#define BOTTOMVIEW 	"0x06"
#define ROMA 		"0x00"
#define ROMB 		"0x04"
#define ROMC 		"0x0C"

void InitDisplay(SPI_HandleTypeDef *hspi);
void ResetDisplay(void);
void ClearDisplay(SPI_HandleTypeDef *hspi);
void SetContrast(SPI_HandleTypeDef *hspi, uint8_t dim);
void SetView(SPI_HandleTypeDef *hspi, uint8_t view);
void SetROM(SPI_HandleTypeDef *hspi, uint8_t rom);
void ControlDisplay(SPI_HandleTypeDef *hspi, uint8_t mode);
void FadeOffMode(SPI_HandleTypeDef *hspi,uint8_t mode);
void DefineCharacter(SPI_HandleTypeDef *hspi, uint8_t memposition, unsigned char *data);
void WriteString(SPI_HandleTypeDef *hspi, char *data);
void WriteStringPosition(SPI_HandleTypeDef *hspi, char *data,uint8_t row,uint8_t col);
void WriteChar(SPI_HandleTypeDef *hspi, char character);
void SetPosition(SPI_HandleTypeDef *hspi,uint8_t row, uint8_t column);

#endif /* OLEDM204_H_ */
