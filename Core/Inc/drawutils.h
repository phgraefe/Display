//-----------------------------------------------------
//File: drawutils.h
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifndef __DRAWUTILS_H
#define __DRAWUTILS_H

void DrawDigitBig(uint8_t digit, uint8_t x);
void DrawNumberBig(uint8_t number, uint8_t x);
void DrawStat2(char *statStr, char *unitStr, char value, uint8_t x, uint8_t y);
void DrawStat3(char *statStr, char *unitStr, char *value, uint8_t x, uint8_t y);
void DrawAMS(void);
void DrawIMD(void);
void DrawTVSelect(uint8_t TV);
void DrawTCSelect(uint8_t TC);
void DrawRecuSelect(uint8_t Recu);
void InitDigits(void);
void DrawGuiFsd(uint8_t mission, uint8_t state);


#endif
