//-----------------------------------------------------
//File: gui.h
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "drawutils.h"

#ifndef INC_GUI_H_
#define INC_GUI_H_

//void DrawGuiFsd(uint8_t mission, uint8_t state);
void DrawGuiFse1(uint8_t TV, uint8_t temp, uint8_t energy, uint8_t recu, uint8_t speed);
void DrawGuiFse2(uint16_t Uaccu, uint16_t Umax, uint16_t Umin, uint8_t temp, uint8_t SC, uint8_t BOTS);
void DrawGuiFse3(uint8_t BrPrFr, uint8_t BrPrRe, uint8_t Acc1, uint8_t Acc2, uint8_t Recu1, uint8_t Recu2);

#endif /* INC_GUI_H_ */
