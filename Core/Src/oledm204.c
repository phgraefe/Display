//-----------------------------------------------------
//File: oledm204.c
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------

//---Includes---
#include "inttypes.h"
#include "stm32f3xx_hal.h"
#include "oledm204.h"

static void WriteIns(SPI_HandleTypeDef *hspi , uint8_t byte);
static void WriteData(SPI_HandleTypeDef *hspi, uint8_t byte);
//-----------------------------------------------------
//Func: initDispl()
//Desc: initialize the display
//-----------------------------------------------------
void InitDisplay(SPI_HandleTypeDef *hspi)
{
	ResetDisplay();
	WriteIns(hspi,0x3A);	// FunctionSet: N=1 BE=0 RE=1 IS=0
	WriteIns(hspi,0x09);	// 4-line mode
	WriteIns(hspi,0x05);	// View: top view=0x05; bottom view=0x06
	WriteIns(hspi,0x3A);	// FunctionSet: N=1 BE=0 RE=1 IS=0
	WriteIns(hspi,0x72);	// ROM Selection (RE muss 1 sein)
	WriteData(hspi,0x00);// ROM_A : 0x00 ROM_B : 0x04 ROM_C : 0x0C
	WriteIns(hspi,0x38);	// FunctionSet: N=1 DH=0 RE=0 IS=0
	WriteIns(hspi,0x0C);	// set display mode cursor, blink on off etc.
	WriteIns(hspi,0x01);	// clear display*/
}
//-----------------------------------------------------
//Func: ResetDisplay
//Desc: Resets the Display
//-----------------------------------------------------
void ResetDisplay(void)
{
	OLED_CS_HIGH;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET);
	HAL_Delay(50);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_Delay(1);
}
//-----------------------------------------------------
//Func: ClearDisplay
//Desc: Clears entire Display content and set home pos
//-----------------------------------------------------
void ClearDisplay(SPI_HandleTypeDef *hspi)
{
	WriteIns(hspi,0x01);
	WriteIns(hspi,0x80);
	HAL_Delay(100);
}
//-----------------------------------------------------
//Func: SetContrast
//Desc:  Changes the contrast 0-255
//-----------------------------------------------------
void SetContrast(SPI_HandleTypeDef *hspi, uint8_t dim)
{
	WriteIns(hspi,0x2A);
	WriteIns(hspi,0x79);	// enable OLED characterization bit
	WriteIns(hspi,0x81);	// set contrast command
	WriteIns(hspi,dim);	// set contrast value
	WriteIns(hspi,0x2A);
	WriteIns(hspi,0x78); // disable OLED characterization bit
}
//-----------------------------------------------------
//Func: SetView
//Desc: view bottom view(0x06), top view (0x05)
//-----------------------------------------------------
void SetView(SPI_HandleTypeDef *hspi, uint8_t view)
{
	// VIEW Bottom: 0x06 Top: 0x05 use #defines BOTTOMVIEW and TOPVIEW
	WriteIns(hspi,0x3A);
	WriteIns(hspi,view);
	WriteIns(hspi,0x38);
}
//-----------------------------------------------------
//Func: SetROM
//Desc: Changes the Rom code (ROMA=0x00, ROMB=0x04, ROMC=0x0C)
//---------------------------------------------------
void SetROM(SPI_HandleTypeDef *hspi, uint8_t rom)
{
	WriteIns(hspi,0x2A);	// Extended Command
	WriteIns(hspi,0x79);	// test sd = 1
	WriteIns(hspi,0x72);	// ROM Selection (RE muss 1 sein)
	WriteData(hspi,rom);
	WriteIns(hspi,0x28);
}
//-----------------------------------------------------
//Func: ControlDisplay
//Desc: use definitions of header file to set display
//-----------------------------------------------------
void ControlDisplay(SPI_HandleTypeDef *hspi, uint8_t mode)
{
	if (mode == 1){
		WriteIns(hspi,0x0C);	// set display mode cursor, blink on off etc.
	} else {
		WriteIns(hspi,0x08);	// set display mode cursor, blink on off etc.
	}
}
//-----------------------------------------------------
//Func: FadeOffMode(mode)
//Desc: sends a string to display
//-----------------------------------------------------
void FadeOffMode(SPI_HandleTypeDef *hspi,uint8_t mode)
{
	if (mode == 1){
		WriteIns(hspi,0x3A);	// Set "RE"=1
		WriteIns(hspi,0x78);	// Set "SD"=0
		WriteIns(hspi,0x10);	// enable fade out
	} else {
		WriteIns(hspi,0x3A);	// Set "RE"=1
		WriteIns(hspi,0x78);	// Set "SD"=0
		WriteIns(hspi,0x00); // disable fade out
	}

	WriteIns(hspi,0x72);	// ROM Selection (RE muss 1 sein)
	WriteData(hspi,0x00);// ROM_A : 0x00 ROM_B : 0x04 ROM_C : 0x0C
}
//-----------------------------------------------------
//Func: DefineCharacter(string)
//Desc: Defines a custom character
//-----------------------------------------------------
void DefineCharacter(SPI_HandleTypeDef *hspi, uint8_t memposition, unsigned char *data)
{
	unsigned char i=0;
	WriteIns(hspi, 0x40+8*memposition);

	for(i = 0; i < 8; i++)
	{
		WriteData(hspi, data[i]);
	}
}
//-----------------------------------------------------
//Func: WriteString(string)
//Desc: sends a string to display
//-----------------------------------------------------
void WriteString(SPI_HandleTypeDef *hspi, char *data)
{
	int i;
	unsigned char buffer[5];

	uint8_t rdata[4] = {0, 0, 0, 0};

	OLED_CS_HIGH;
	OLED_CS_LOW;

	while (*data) {
		buffer[0] = 0x5F;
		buffer[1] = (*data) 		& 0x0F;
		buffer[2] = (*data>>4) 	& 0x0F;
		HAL_SPI_TransmitReceive(hspi, buffer, rdata, 3, 0xFF);
		i++;
		data++;
	}
	OLED_CS_HIGH;
}
//-----------------------------------------------------
//Func:	WriteStringPosition(string, row, col)
//Desc: sends a single character to display
//-----------------------------------------------------
void WriteStringPosition(SPI_HandleTypeDef *hspi, char *data,uint8_t row,uint8_t col)
{
	SetPosition(hspi,row, col);
	WriteString(hspi,data);
}
//-----------------------------------------------------
//Func: WriteChar(character)
//Desc: sends a single character to display
//-----------------------------------------------------
void WriteChar(SPI_HandleTypeDef *hspi, char character)
{
	WriteData(hspi, character);
}
//-----------------------------------------------------
//Func: SetPostion(row, column)
//Desc: set postion
//-----------------------------------------------------
void SetPosition(SPI_HandleTypeDef *hspi,uint8_t row, uint8_t column)
{
	uint8_t line_offset;
	  switch (row) {
	    case 0:  line_offset = 0x00; break;  // Line 1
	    case 1:  line_offset = 0x20; break;  // LIne 2
	    case 2:  line_offset = 0x40; break;  // Line 3
	    case 3:  line_offset = 0x60; break;  // Line 4
	    default: line_offset = 0x60;         // line>3
	  }
	  WriteIns(hspi,0x80+line_offset + column);
}
//-----------------------------------------------------
//Func: WriteIns(instruction)
//Desc: sends instruction to display
//-----------------------------------------------------
static void WriteIns(SPI_HandleTypeDef *hspi , uint8_t byte)
{
	unsigned char bufferx[5];
	uint8_t rdata[4] = {0, 0, 0, 0};
	bufferx[0] = 0x1F;
	bufferx[1] = byte 		& 0x0F;
	bufferx[2] = byte>>4 	& 0x0F;
	OLED_CS_HIGH;
	OLED_CS_LOW;
	HAL_SPI_TransmitReceive(hspi, bufferx, rdata, 3, 0xFF);
	OLED_CS_HIGH;
}
//-----------------------------------------------------
//Func: WriteData(data)
//Desc: sends data to display
//-----------------------------------------------------
static void WriteData(SPI_HandleTypeDef *hspi, uint8_t byte)
{
	unsigned char bufferx[5];
	uint8_t rdata[4] = {0, 0, 0, 0};
	bufferx[0] = 0x5F;
	bufferx[1] = byte 		& 0x0F;
	bufferx[2] = byte>>4 	& 0x0F;
	OLED_CS_HIGH;	// the CS signal is essential in EACH command to the display, not just at a the beginning of a programming sequence
	OLED_CS_LOW;
	HAL_SPI_TransmitReceive(hspi, bufferx, rdata, 3, 0xFF);
	OLED_CS_HIGH;
}




