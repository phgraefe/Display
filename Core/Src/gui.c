//-----------------------------------------------------
//File: gui.c
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------

#include "gui.h"

/*void DrawGuiFsd(uint8_t mission, uint8_t state)
{
	WriteString(&hspi3,"Mission: ");
	SetPosition(&hspi3,1, 0);
    switch (mission)
    {
    case 0:
        WriteString(&hspi3,"None");
        break;
    case 1:
    	WriteString(&hspi3,"Acceleration");
        break;
    case 2:
    	WriteString(&hspi3,"Skidpad     ");
        break;
    case 3:
    	WriteString(&hspi3,"Trackdrive  ");
        break;
    case 4:
    	WriteString(&hspi3,"Braketest   ");
        break;
    case 5:
    	WriteString(&hspi3,"Inspection  ");
        break;
    case 6:
    	WriteString(&hspi3,"Autocross   ");
        break;
    case 7:
    	WriteString(&hspi3,"Manual      ");
        break;
    default:
    	WriteString(&hspi3,"Faulty ID   ");
        break;
    }
    SetPosition(&hspi3,2, 0);
    WriteString(&hspi3,"State: ");
    SetPosition(&hspi3,3, 0);
    switch (state)
        {
        case 0:
        	WriteString(&hspi3,"Select         ");
            break;
        case 1:
        	WriteString(&hspi3,"Check          ");
            break;
        case 2:
        	WriteString(&hspi3,"Ready          ");
            break;
        case 3:
        	WriteString(&hspi3,"Driving        ");
            break;
        case 4:
        	WriteString(&hspi3,"Finish         ");
            break;
        case 5:
        	WriteString(&hspi3,"Emergency Brake");
            break;
        default:
        	WriteString(&hspi3,"Faulty ID      ");
            break;
        }
}*/
void DrawGuiFse1(uint8_t TV, uint8_t TC, uint8_t recu, uint8_t energy, uint8_t speed)
{
    DrawStat2("TV", "", TV, 0, 0);
    DrawStat2("TC", "", TC, 0, 1);
    DrawStat2("REC", "", recu, 0, 2);
    DrawStat2("SOC", "%", energy, 0, 3);
    DrawNumberBig(speed, 20 - 11);
}
void DrawGuiFse2(uint16_t Uaccu, uint16_t Umax, uint16_t Umin, uint8_t temp, uint8_t SC, uint8_t BOTS)
{
	DrawStat3("U", "V", Uaccu, 0, 0);
	DrawStat2("SC", "", SC, 15, 0);
	DrawStat3("Umax", "mV", Umax, 0, 1);
	DrawStat3("Umin", "mV", Umin, 0, 2);
	DrawStat2("Tmax", "C", temp, 0, 3);
	DrawStat2("BOTS", "", BOTS, 13, 3);
}
void DrawGuiFse3(uint8_t BrPrFr, uint8_t BrPrRe, uint8_t Acc1, uint8_t Acc2, uint8_t Recu1, uint8_t Recu2)
{
	DrawStat3("BrkPrFr", "Bar", BrPrFr, 3, 0);
	DrawStat3("BrkPrRe", "Bar", BrPrRe, 3, 1);
	DrawStat3("Acc1", "%", Acc1, 0, 2);
	DrawStat3("Acc2", "%", Acc2, 0, 3);
	DrawStat3("Recu1", "%", Recu1, 10, 2);
	DrawStat3("Recu2", "%", Recu2, 10, 3);
}
