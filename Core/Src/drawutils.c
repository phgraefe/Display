//-----------------------------------------------------
//File: drawutils.c
//Auth: Philipp Gräfe
//DATE: 03.02.2021
//-----------------------------------------------------

#include "drawutils.h"
#include "stm32f3xx_hal.h"
#include "spi.h"
#include "oledm204.h"

//A
uint8_t A[4][4] = {
	{0x20, 0x1F, 0x1F, 0x20},
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x1F, 0x1F, 0x1F},
	{0x1F, 0x20, 0x20, 0x1F}};

//C
uint8_t C[4][4] = {
	{0x1F, 0x1F, 0x1F, 0x1F},
	{0x1F, 0x20, 0x20, 0x20},
	{0x1F, 0x20, 0x20, 0x20},
	{0x1F, 0x1F, 0x1F, 0x1F}};

//D
uint8_t D[4][3] = {
	{0x1F, 0x1F, 0x20},
	{0x1F, 0x20, 0x1F},
	{0x1F, 0x20, 0x1F},
	{0x1F, 0x1F, 0x20}};

//I
uint8_t I[4][1] = {
	{0x1F},
	{0x1F},
	{0x1F},
	{0x1F}};

//M
uint8_t M[4][4] = {
	{0x1F, 0x02, 0x03, 0x1F},
	{0x1F, 0x04, 0x05, 0x1F},
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x20, 0x20, 0x1F}};

//R
uint8_t R[4][4] = {
	{0x1F, 0x1F, 0x1F, 0x20},
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x1F, 0x1F, 0x20},
	{0x1F, 0x20, 0x20, 0x1F}};

//S
uint8_t S[4][4] = {
	{0x1F, 0x1F, 0x1F, 0x1F},
	{0x1F, 0x00, 0x00, 0x00},
	{0x01, 0x01, 0x01, 0x1F},
	{0x1F, 0x1F, 0x1F, 0x1F}};

//T
uint8_t T[4][5] = {
	{0x1F, 0x1F, 0x1F, 0x1F, 0x1F},
	{0x20, 0x20, 0x1F, 0x20, 0x20},
	{0x20, 0x20, 0x1F, 0x20, 0x20},
	{0x20, 0x20, 0x1F, 0x20, 0x20}};

//U
uint8_t U[4][4] = {
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x20, 0x20, 0x1F},
	{0x1F, 0x1F, 0x1F, 0x1F}};

//V
uint8_t V[4][5] = {
	{0x1F, 0x20, 0x20, 0x20, 0x1F},
	{0x20, 0x1F, 0x20, 0x1F, 0x20},
	{0x20, 0x1F, 0x20, 0x1F, 0x20},
	{0x20, 0x20, 0x1F, 0x20, 0x20}};

uint8_t digits[10][4][3] = {
    //0
    {{0x1F, 0x1F, 0x1F},
     {0x1F, 0x20, 0x1F},
     {0x1F, 0x20, 0x1F},
     {0x1F, 0x1F, 0x1F}},

    //1
    {{0x1F, 0x1F, 0x20},
     {0x20, 0x1F, 0x20},
     {0x20, 0x1F, 0x20},
     {0x1F, 0x1F, 0x1F}},

    //2
    {{0x1F, 0x1F, 0x1F},
     {0x00, 0x00, 0x1F},
     {0x1F, 0x01, 0x01},
     {0x1F, 0x1F, 0x1F}},

    //3
    {{0x1F, 0x1F, 0x1F},
     {0x20, 0x00, 0x1F},
     {0x20, 0x01, 0x1F},
     {0x1F, 0x1F, 0x1F}},

    //4
    {{0x1F, 0x20, 0x1F},
     {0x1F, 0x20, 0x1F},
     {0xD6, 0xD6, 0x1F},
     {0x20, 0x20, 0x1F}},

    //5
    {{0x1F, 0x1F, 0x1F},
     {0x1F, 0x00, 0x00},
     {0x01, 0x01, 0x1F},
     {0x1F, 0x1F, 0x1F}},

    //6
    {{0x1F, 0x1F, 0x1F},
     {0x1F, 0x00, 0x00},
     {0x1F, 0x01, 0x1F},
     {0x1F, 0x1F, 0x1F}},

    //7
    {{0x1F, 0x1F, 0x1F},
     {0x20, 0x20, 0x1F},
     {0x20, 0x20, 0x1F},
     {0x20, 0x20, 0x1F}},

    //8
    {{0x1F, 0x1F, 0x1F},
     {0x1F, 0x00, 0x1F},
     {0x1F, 0x01, 0x1F},
     {0x1F, 0x1F, 0x1F}},

    //9
    {{0x1F, 0x1F, 0x1F},
     {0x1F, 0x00, 0x1F},
     {0x01, 0x01, 0x1F},
     {0x1F, 0x1F, 0x1F}}};
//-----------------------------------------------------
//Func: DrawDigitBig(digit, x-position)
//Desc: Draws a big single digit
//-----------------------------------------------------
void DrawDigitBig(uint8_t digit, uint8_t x1)
{
    if (digit > 9)
        return;

    for (uint8_t y = 0; y < 4; y++)
    {
    	SetPosition(&hspi3, y, x1);
    	WriteChar(&hspi3, digits[digit][y][0]);
    	WriteChar(&hspi3, digits[digit][y][1]);
    	WriteChar(&hspi3, digits[digit][y][2]);
    }
}
//-----------------------------------------------------
//Func: DrawNumberBig(number, x-position)
//Desc: Draws a big three digit number
//-----------------------------------------------------
void DrawNumberBig(uint8_t number, uint8_t x1)
{
    char numberStr[] = "    ";
    sprintf(numberStr, "%03d", number);

    for (int y = 0; y < 4; y++)
    {
    	SetPosition(&hspi3, y, x1);
        for (int x = 0, i = 0; x < 4 * strlen(numberStr); x += 4, i++)
        {
            if (i != 0)
            	WriteChar(&hspi3, 0x20);
            if (numberStr[i] == ' ')
            {
            	WriteChar(&hspi3, 0x20);
            	WriteChar(&hspi3, 0x20);
            	WriteChar(&hspi3, 0x20);
            }
            else
            {
            	WriteChar(&hspi3, digits[numberStr[i] - 48][y][0]);
            	WriteChar(&hspi3, digits[numberStr[i] - 48][y][1]);
            	WriteChar(&hspi3, digits[numberStr[i] - 48][y][2]);
            }
        }
    }
}

//-----------------------------------------------------
//Func: DrawStat2()
//Desc: Draws a big three digit number
//-----------------------------------------------------
void DrawStat2(char *statStr, char *unitStr, char value, uint8_t x, uint8_t y)
{
	SetPosition(&hspi3, y, x);
	WriteString(&hspi3, statStr);
	char valueStr[] = "    ";
	sprintf(valueStr, ":%2d", value);
	WriteString(&hspi3, valueStr);
	WriteString(&hspi3, unitStr);
}
//-----------------------------------------------------
//Func: DrawStat2()
//Desc: Draws a big three digit number
//-----------------------------------------------------
void DrawStat3(char *statStr, char *unitStr, char *value, uint8_t x, uint8_t y)
{
	SetPosition(&hspi3, y, x);
	WriteString(&hspi3, statStr);
	char valueStr[] = "    ";
	sprintf(valueStr, ":%d", value);
	WriteString(&hspi3, valueStr);
	WriteString(&hspi3, unitStr);
}
//-----------------------------------------------------
//Func: DrawAMS()
//Desc: Draws AMS at the Display
//-----------------------------------------------------
void DrawAMS()
{
	for (uint8_t y = 0; y < 4; y++)
	    	{
	    	SetPosition(&hspi3, y, 3);
	    	WriteChar(&hspi3, A[y][0]);
	    	WriteChar(&hspi3, A[y][1]);
	    	WriteChar(&hspi3, A[y][2]);
	    	WriteChar(&hspi3, A[y][3]);
	    	}
	for (uint8_t y = 0; y < 4; y++)
		    {
		    SetPosition(&hspi3, y, 8);
		    WriteChar(&hspi3, M[y][0]);
		    WriteChar(&hspi3, M[y][1]);
		    WriteChar(&hspi3, M[y][2]);
		    WriteChar(&hspi3, M[y][3]);
		    }
	for (uint8_t y = 0; y < 4; y++)
		    {
		    SetPosition(&hspi3, y, 13);
		    WriteChar(&hspi3, S[y][0]);
		    WriteChar(&hspi3, S[y][1]);
		    WriteChar(&hspi3, S[y][2]);
		    WriteChar(&hspi3, S[y][3]);
		    }
}
//-----------------------------------------------------
//Func: DrawIMD()
//Desc: Draws IMD at the Display
//-----------------------------------------------------
void DrawIMD()
{
	for (uint8_t y = 0; y < 4; y++)
	    	{
	    	SetPosition(&hspi3, y, 5);
	    	WriteChar(&hspi3, I[y][0]);
	    	}
	for (uint8_t y = 0; y < 4; y++)
		    {
		    SetPosition(&hspi3, y, 7);
		    WriteChar(&hspi3, M[y][0]);
		    WriteChar(&hspi3, M[y][1]);
		    WriteChar(&hspi3, M[y][2]);
		    WriteChar(&hspi3, M[y][3]);
		    }
	for (uint8_t y = 0; y < 4; y++)
		    {
		    SetPosition(&hspi3, y, 12);
		    WriteChar(&hspi3, D[y][0]);
		    WriteChar(&hspi3, D[y][1]);
		    WriteChar(&hspi3, D[y][2]);
		    }
}
//-----------------------------------------------------
//Func: DrawTVSelect(TV)
//Desc: Draws TV + digit
//-----------------------------------------------------
void DrawTVSelect(uint8_t TV)
{
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 2);
	 		WriteChar(&hspi3, T[y][0]);
	 		WriteChar(&hspi3, T[y][1]);
	 		WriteChar(&hspi3, T[y][2]);
	 		WriteChar(&hspi3, T[y][3]);
	 		WriteChar(&hspi3, T[y][4]);
	 		}
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 8);
	 		WriteChar(&hspi3, V[y][0]);
	 		WriteChar(&hspi3, V[y][1]);
	 		WriteChar(&hspi3, V[y][2]);
	 		WriteChar(&hspi3, V[y][3]);
	 		WriteChar(&hspi3, V[y][4]);
		    }
	 DrawDigitBig(TV, 15);
}
//-----------------------------------------------------
//Func: DrawTCSelect(TC)
//Desc: Draws TC + digit
//-----------------------------------------------------
void DrawTCSelect(uint8_t TC)
{
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 2);
	 		WriteChar(&hspi3, T[y][0]);
	 		WriteChar(&hspi3, T[y][1]);
	 		WriteChar(&hspi3, T[y][2]);
	 		WriteChar(&hspi3, T[y][3]);
	 		WriteChar(&hspi3, T[y][4]);
	 		}
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 8);
	 		WriteChar(&hspi3, C[y][0]);
	 		WriteChar(&hspi3, C[y][1]);
	 		WriteChar(&hspi3, C[y][2]);
	 		WriteChar(&hspi3, C[y][3]);
		    }
	 DrawDigitBig(TC, 15);
}
//-----------------------------------------------------
//Func: DrawRecuSelect(Recu)
//Desc: Draws Recu + digit
//-----------------------------------------------------
void DrawRecuSelect(uint8_t Recu)
{
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 0);
	 		WriteChar(&hspi3, R[y][0]);
	 		WriteChar(&hspi3, R[y][1]);
	 		WriteChar(&hspi3, R[y][2]);
	 		WriteChar(&hspi3, R[y][3]);

	 		}
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 5);
	 		WriteChar(&hspi3, C[y][0]);
	 		WriteChar(&hspi3, C[y][1]);
	 		WriteChar(&hspi3, C[y][2]);
	 		WriteChar(&hspi3, C[y][3]);
		    }
	 for (uint8_t y = 0; y < 4; y++)
	 		{
	 		SetPosition(&hspi3, y, 10);
	 		WriteChar(&hspi3, U[y][0]);
	 		WriteChar(&hspi3, U[y][1]);
	 		WriteChar(&hspi3, U[y][2]);
	 		WriteChar(&hspi3, U[y][3]);
		    }
	 DrawDigitBig(Recu, 16);
}
//-----------------------------------------------------
//Func: DrawRecuSelect(Recu)
//Desc: Draws Recu + digit
//-----------------------------------------------------
void InitDigits()
{
	  unsigned char down[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x1F};
	  unsigned char up[] = {0x1F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	  unsigned char MLO[] = {0x00, 0x10, 0x10, 0x18, 0x18, 0x1C, 0x1C, 0x1E};
	  unsigned char MRO[] = {0x00, 0x01, 0x01, 0x03, 0x03, 0x07, 0x07, 0x0F};
	  unsigned char MLU[] = {0x0F, 0x0F, 0x07, 0x07, 0x03, 0x03, 0x01, 0x00};
	  unsigned char MRU[] = {0x1E, 0x1E, 0x1C, 0x1C, 0x18, 0x18, 0x10, 0x00};
	  InitDisplay(&hspi3);
	  DefineCharacter(&hspi3, 0x00, down);
	  DefineCharacter(&hspi3, 0x01, up);
	  DefineCharacter(&hspi3, 0x02, MLO);
	  DefineCharacter(&hspi3, 0x03, MRO);
	  DefineCharacter(&hspi3, 0x04, MLU);
	  DefineCharacter(&hspi3, 0x05, MRU);
	  InitDisplay(&hspi3);
}
void DrawGuiFsd(uint8_t mission, uint8_t state)
{
	WriteString(&hspi3,"Mission: ");
	SetPosition(&hspi3,1, 0);
    switch (mission)
    {
    case 0:
        WriteString(&hspi3,"None");
        break;
    case 1:
    	WriteString(&hspi3,"Acceleration");
        break;
    case 2:
    	WriteString(&hspi3,"Skidpad     ");
        break;
    case 3:
    	WriteString(&hspi3,"Trackdrive  ");
        break;
    case 4:
    	WriteString(&hspi3,"Braketest   ");
        break;
    case 5:
    	WriteString(&hspi3,"Inspection  ");
        break;
    case 6:
    	WriteString(&hspi3,"Autocross   ");
        break;
    case 7:
    	WriteString(&hspi3,"Manual      ");
        break;
    default:
    	WriteString(&hspi3,"Faulty ID   ");
        break;
    }
    SetPosition(&hspi3,2, 0);
    WriteString(&hspi3,"State: ");
    SetPosition(&hspi3,3, 0);
    switch (state)
        {
        case 0:
        	WriteString(&hspi3,"Select         ");
            break;
        case 1:
        	WriteString(&hspi3,"Check          ");
            break;
        case 2:
        	WriteString(&hspi3,"Ready          ");
            break;
        case 3:
        	WriteString(&hspi3,"Driving        ");
            break;
        case 4:
        	WriteString(&hspi3,"Finish         ");
            break;
        case 5:
        	WriteString(&hspi3,"Emergency Brake");
            break;
        default:
        	WriteString(&hspi3,"Faulty ID      ");
            break;
        }
}
